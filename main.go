package main

import (
	"github.com/go-redis/redis"
	"github.com/it-sgcu/rnkm-transit/population/broker"
	"github.com/it-sgcu/rnkm-transit/population/constant"
	"github.com/it-sgcu/rnkm-transit/population/db"
	"github.com/it-sgcu/rnkm-transit/population/population"
)

func main() {
	constant.LoadConfig()
	redisConn := db.NewRedis()
	redisConn.Init(&redis.Options{
		Addr:     constant.Config.RedisHost,
		Password: "",
		DB:       0,
	})
	kafkaBroker := broker.NewKafkaBroker(constant.Config.KafkaBrokers)
	t := population.NewWatcher(kafkaBroker, redisConn)
	t.Watch()
}
