#! /bin/bash

# create
sudo docker run -p 6379:6379 --name some-redis -d redis redis-server --appendonly yes || {
    # or start if already created
    docker start some-redis
}

