package constant

import "github.com/segmentio/kafka-go"

var KeyPartitionMap = map[string]int{
	// population topic key map
	"population": 0,
}

// TransitBalancer : transit kafka key to partition mappers
type TransitBalancer struct{}

// Balance : partition selector from message key
func (tb *TransitBalancer) Balance(msg kafka.Message, partitions ...int) int {
	if partition, exist := KeyPartitionMap[string(msg.Key)]; exist {
		return partition
	}
	return 0
}
