package broker

import (
	"context"
	"log"
	"os"
	"time"

	"github.com/it-sgcu/rnkm-transit/population/constant"
	"github.com/pkg/errors"
	"github.com/segmentio/kafka-go"
)

// KafkaBroker simple implementation of Kafka (no partiton yet)
type KafkaBroker struct {
	populationProducer *kafka.Writer
	logger             *log.Logger
	balancer           kafka.Balancer
	stop               bool
	addr               []string
}

// NewKafkaBroker : create kafka broker
func NewKafkaBroker(address []string) *KafkaBroker {
	b := &KafkaBroker{
		stop:     false,
		addr:     address,
		logger:   log.New(os.Stdout, "[Broker]", log.LstdFlags),
		balancer: &constant.TransitBalancer{},
	}
	b.populationProducer = kafka.NewWriter(kafka.WriterConfig{
		Brokers:      address,
		Topic:        "population",
		Balancer:     b.balancer,
		RequiredAcks: 1,
		BatchTimeout: 10 * time.Millisecond,
	})
	return b
}

func truncate(s []byte, length int) []byte {
	if len(s) < length {
		return s
	}
	return s[:length]
}

func (b *KafkaBroker) logError(msg string, err error) {
	b.logger.SetOutput(os.Stderr)
	b.logger.Println(msg)
	if err != nil {
		b.logger.Printf("%+v\n", errors.WithStack(err))
	}
	b.logger.SetOutput(os.Stdout)
}

// Publish : publish message with value: `value` in to topic `topic` (I might add key in future ?)
func (b *KafkaBroker) Publish(topic, key string, value []byte) error {
	b.logger.Printf("[%s] ---> %s... \n", topic, truncate(value, 20))
	return b.populationProducer.WriteMessages(context.Background(),
		kafka.Message{
			Key:   []byte(key),
			Value: value,
		},
	)
}
