package broker

// Broker : transit & population message broker
type Broker interface {
	Publish(topic, key string, message []byte) error // Publish to broker
}
