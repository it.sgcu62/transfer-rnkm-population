package population

import (
	"github.com/it-sgcu/rnkm-transit/population/constant"
	baanModel "github.com/it-sgcu/rnkm-transit/population/model/baan"
)

// State : subscribe for `worldPopulation` topic and publish `diff` to `population` topic
type State struct {
	latestState baanModel.WorldPopulations
	commitNo    int
}

// NewPopulationState : Create new population state
func NewPopulationState() *State {
	facultyCount := len(constant.FacultyList)
	newState := &State{
		latestState: make(baanModel.WorldPopulations),
		commitNo:    0,
	}
	for baan, limit := range constant.MockBaanLimit {
		currentPeople := 0

		facultyPopulation := make([]int, facultyCount)
		for i := range facultyPopulation {
			facultyPopulation[i] = 0
			currentPeople += facultyPopulation[i]
		}

		female := 0
		newState.latestState[baan] = baanModel.Population{
			Limit:   limit,
			Current: currentPeople,
			Faculty: facultyPopulation,
			Gender: [2]int{
				female,                 // index:0, female
				currentPeople - female, // index:1, male
			},
		}
	}
	return newState
}

// SetState (*Watcher): Set new baan population status, return JSON form of change between old & new state
func (w *State) SetState(newPopulation *baanModel.WorldPopulations) *State {
	diff := make(baanModel.WorldPopulations)
	for baan, oldBaanPopulation := range w.latestState {
		newBaanPopulation := (*newPopulation)[baan]
		facultyDiff := make([]int, len(oldBaanPopulation.Faculty))
		for i, v := range newBaanPopulation.Faculty {
			facultyDiff[i] = v - oldBaanPopulation.Faculty[i]
		}
		diff[baan] = baanModel.Population{
			Limit:   newBaanPopulation.Limit - oldBaanPopulation.Limit,
			Current: newBaanPopulation.Current - oldBaanPopulation.Current,
			Faculty: facultyDiff,
			Gender: [2]int{
				newBaanPopulation.Gender[0] - oldBaanPopulation.Gender[0],
				newBaanPopulation.Gender[1] - oldBaanPopulation.Gender[1],
			},
		}
	}
	w.latestState = *newPopulation
	if !diff.IsAllZero() {
		w.commitNo++
		return &State{
			commitNo:    w.commitNo,
			latestState: diff,
		}
	}
	return nil
}

// LatestState : get latest world population state
func (w *State) LatestState() map[string]interface{} {
	return map[string]interface{}{
		"cn": w.commitNo,
		"s":  w.latestState,
	}
}
