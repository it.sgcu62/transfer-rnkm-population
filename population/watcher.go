package population

import (
	"encoding/json"
	"log"
	"os"
	"time"

	"github.com/pkg/errors"

	"github.com/it-sgcu/rnkm-transit/population/broker"
	"github.com/it-sgcu/rnkm-transit/population/db"
)

const STRING_WIDTH = 3 // Width of enconding in parseStringArray and encodeStringArray

type Watcher struct {
	broker broker.Broker
	redis  *db.Redis
	logger *log.Logger
	state  *State // use state to conveniently diff (and reuse code)
}

func (w *Watcher) logError(msg string, err error) {
	w.logger.SetOutput(os.Stderr)
	w.logger.Println(msg)
	if err != nil {
		w.logger.Printf("%+v\n", errors.WithStack(err))
	}
	w.logger.SetOutput(os.Stdout)
}

// NewWatcher create new redis client with connection to `broker`
func NewWatcher(broker broker.Broker, redis *db.Redis) *Watcher {
	return &Watcher{
		broker: broker,
		redis:  redis,
		logger: log.New(os.Stdout, "[Watcher]", log.Lshortfile),
		state:  NewPopulationState(),
	}
}

// Watch start watching database and send current baan populations: TODO: Diff
func (w *Watcher) Watch() {
	for {
		time.Sleep(100 * time.Millisecond)
		pop, err := w.redis.GetAllBaans()
		if err != nil {
			w.logError("GetAllBaans error", err)
			continue
		}
		diff := w.state.SetState(pop)
		if diff != nil {
			str, _ := json.Marshal(pop)
			w.broker.Publish("population", "population", str) // TODO: how to publish ?
		}
	}
}
