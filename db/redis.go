package db

import (
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"

	modelBaan "github.com/it-sgcu/rnkm-transit/population/model/baan"
	"github.com/pkg/errors"

	"github.com/go-redis/redis"
	"github.com/it-sgcu/rnkm-transit/population/constant"
)

const STRING_WIDTH = 3 // Width of enconding in parseStringArray and encodeStringArray

type Redis struct {
	client *redis.Client
	logger *log.Logger
}

// NewRedis create new redis client with connection to `broker`
func NewRedis() *Redis {
	return &Redis{
		logger: log.New(os.Stdout, "[Redis]", log.Lshortfile),
	}
}

func (r *Redis) logError(msg string, err error) {
	r.logger.SetOutput(os.Stderr)
	r.logger.Println(msg)
	if err != nil {
		r.logger.Printf("%+v\n", errors.WithStack(err))
	}
	r.logger.SetOutput(os.Stdout)
}

// prepareDB ensure that all baan's are in HASH and is initialized
func (r *Redis) prepareDB() error {
	batch := r.client.Pipeline()
	facultyZero := "" // represent all zero in each faculty
	for i := 0; i < len(constant.FacultyList); i++ {
		facultyZero += "  0"
	}
	for _, baan := range constant.BaanList {
		batch.HSetNX("population/"+baan, "limit", constant.MockBaanLimit[baan])
		batch.HSetNX("population/"+baan, "current", "0")
		batch.HSetNX("population/"+baan, "faculty", facultyZero) // 3 digit each
		batch.HSetNX("population/"+baan, "gender", "  0  0")     // 0 female and 0 male
	}
	_, err := batch.Exec()
	if err != nil {
		r.logError("Error initializing database %v\n", err)
	} else {
		r.logger.Println("Init database OK")
	}
	return err
}

// Init : initialize redis
func (r *Redis) Init(rOpt *redis.Options) {
	r.client = redis.NewClient(rOpt)
	// r.client = redis.NewFailoverClient(&redis.FailoverOptions{
	// 	MasterName:    "mymaster",
	// 	SentinelAddrs: []string{rOpt.Addr},
	// })
	pong, err := r.client.Ping().Result()
	if err != nil {
		r.logError("error: %v\n", err)
		os.Exit(1)
	}
	r.logger.Printf("%s\n", pong)
	if constant.Config.Dev {
		r.prepareDB()
	}
}

// parseInt: helper to parseInt with spaces leading/behind
func parseInt(str string) (int, error) {
	trimmed := strings.Trim(str, " \t\n")
	return strconv.Atoi(trimmed)
}

// mapJoin = apply mapFunc to each string
func encodeStringArray(array []int, width int) string {
	format := fmt.Sprintf("%%0%dd", width) // %0<width>d
	str := ""
	for _, v := range array {
		str += fmt.Sprintf(format, v)
	}
	return str
}

func parseStringArray(str string, width int) ([]int, error) {
	if len(str)%width != 0 {
		return nil, errors.New("Parsing error []")
	}
	arr := make([]int, len(str)/width)
	for i := range arr {
		num, parseErr := parseInt(str[width*i : width*(i+1)])
		if parseErr != nil {
			return nil, errors.New(fmt.Sprintf("Parse error at %d:%d - [%s] is not int\n", width*i, width*(i+1), str[width*i:width*(i+1)]))
		}
		arr[i] = num
	}
	return arr, nil
}

// parseRedisBaan: convert redis style `map[string]string` to `Population` struct
func parseRedisBaan(baan map[string]string) (population modelBaan.Population, err error) {
	var faculty []int
	var gender []int
	var limit, current int

	limit, err = parseInt(baan["limit"])
	if err != nil {
		err = errors.New(fmt.Sprintf("Error Parsing baan:%s\n offending value = [%s]\n", err, baan["limit"]))
		return
	}
	population.Limit = limit

	current, err = parseInt(baan["current"])
	if err != nil {
		err = errors.New(fmt.Sprintf("Error Parsing baan:%s\n offending value = [%s]\n", err, baan["current"]))
		return
	}
	population.Current = current

	faculty, err = parseStringArray(baan["faculty"], STRING_WIDTH)
	if err != nil {
		err = errors.New(fmt.Sprintf("Error Parsing baan:%s\n offending value = [%s]\n", err, baan["faculty"]))
		return
	}
	population.Faculty = faculty

	gender, err = parseStringArray(baan["gender"], STRING_WIDTH)
	if err != nil {
		err = errors.New(fmt.Sprintf("Error Parsing baan:%s\n offending value = [%s]\n", err, baan["gender"]))
		return
	}
	population.Gender[0] = gender[0]
	population.Gender[1] = gender[1]

	return
}

func (r *Redis) GetBaan(baan string) (modelBaan.Population, error) {
	data, err := r.client.HGetAll("population/" + baan).Result()
	if err != nil {
		return modelBaan.Population{}, err
	} else {
		population, err := parseRedisBaan(data)
		if err != nil {
			return modelBaan.Population{}, err
		}
		return population, nil
	}
}

func (r *Redis) GetAllBaans() (*modelBaan.WorldPopulations, error) { // Get Populations of all Baan's and convert in to go-ish struct
	worldPopulation := make(modelBaan.WorldPopulations)
	for _, baan := range constant.BaanList {
		population, err := r.GetBaan(baan)
		if err != nil {
			return nil, err
		}
		worldPopulation[baan] = population
	}
	return &worldPopulation, nil
}
