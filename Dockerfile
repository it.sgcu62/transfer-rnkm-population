FROM golang:1.12
LABEL maintainer ["Tiger T. Pisnupoomi", "Roadchananat Khunakornophat"]
WORKDIR /go/src/github.com/it-sgcu/rnkm-transit/population
COPY . .
RUN go get ./...
RUN go build -o watcher
CMD ["./watcher"]
